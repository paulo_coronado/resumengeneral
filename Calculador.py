import math


class Calculador:
    
    def __init__(self):
        self.a=0
        self.b=0
        self.r=0
    
    def sumar(self):
        self.r=self.a+self.b
        return True

    def ingresarNumeros(self, x,y):
        self.a=x
        self.b=y
        return True

class CalculadoraCientifica(Calculador):

    def calcularSeno(self):
        self.r= math.sin(self.a)
        return True


miCalculadoraCientifica=CalculadoraCientifica()
miCalculadoraCientifica.ingresarNumeros(math.pi/2,0)
miCalculadoraCientifica.calcularSeno()

print(miCalculadoraCientifica.r)

